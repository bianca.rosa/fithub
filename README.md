## FitHub: Location-Aware Web Application for Fitness Centres

**Run the application:**

1. Install Node.js and PostgreSQL
2. Clone the repository
3. In both frontend and backend folders run `npm install` in order to install the needed dependencies
4. Add the username, password and database credentials to the app-data-source.ts file
5. Create a Google Cloud account and enable the following APIs:
	- Maps JavaScript API
	- Places API
	- Directions API
5. Create a Cloudinary account.
6. Create a .env file in the backend folder and fill in with Cloudinary credentials (CLOUDINARY_NAME, CLOUDINARY_API_KEY, CLOUDINARY_API_SECRET). Provide ACCESS_TOKEN_SECRET variable a value.
7. Create a .env file in the frontend folder and fill in REACT_APP_API_KEY with the secret key provided in the Google Cloud account.
8. Open two terminals, one inside the frontend and the backend folder and run the following command to start the application: `npm start`.
